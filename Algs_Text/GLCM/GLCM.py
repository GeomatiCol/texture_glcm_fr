# -*- coding: utf-8 -*-
"""
GLCM.py Script that calculate a GLCM image of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
Created on Tue Sep 01 19:26:33 2015

@author: DAVID
"""
from Algs_Text.WIN.Windowing import windowing
import numpy as np
from skimage.feature import greycoprops as gcp
from skimage.feature import greycomatrix as gcm
import gdalnumeric
import gdal
import time

def GLCM(img,w,est='correlation',outimg="OutImgText.tif"):
    """Esta función calcula la imagen de texturas por medio de la matriz de 
    co-Ocurrencias (GLCM) de una imagen SAR tamaño MxM
    recibe como parametros la imagen img, el tamaño de ventana w, la estadistica est a
    calcular y el nombre de la imagen de salida\n
    El modulo utilizado para calculo de estadisticas es skimage.feature.greycoprops
    y puede calcular las siguientes estadisticas:\n
    *'homogeneity'\n
    *'contrast'\n
    *'dissimilarity'\n
    *'ASM'\n
    *'energy'\n
    *'correlation'\n
    Por tanto los anteriores son los valores posibles a ser ingresados por 
    el usuario para la variable est, por defecto se calcula la estadistica de 
    correlación. Para complementar lo anterior consultar la documentación 
    del modulo. 
      
    """
    start=time.time()
    srcImg=gdal.Open(img)
    geoTrans=srcImg.GetGeoTransform()
    OriginX=geoTrans[0]
    OriginY=geoTrans[3]
    PixelWidth=geoTrans[1]
    PixelHeight=geoTrans[5]
    NewOriginX=1.0*(((w-1.0)/2.0)*1.0*PixelWidth)+OriginX
    NewOriginY=1.0*(((w-1.0)/2.0)*1.0*PixelHeight)+OriginY
    # se llama a la función windowing 
    # que generara las subimagenes
    win5=windowing(img,w)
    # se obtiene las subimagenes
    win5_arrays=win5[1]
    # se crea una lista 
    ImgText=list()
    # bucle que ira por cada subimagen
    for i in range(win5[-1]):
        # se calcula la DF de cada subimagen,
        # recibe como parametro la subventana
        # y el numero de pasos
        g = gcm(win5_arrays[i], [1], [0], levels=256,normed=True, symmetric=True)
        Homog = gcp(g, est)
        PromH=np.sum(1.0*Homog[0,:])/4#se divide en cuatro para obtener el promedio de las direcciones
        # se imprime el valor de la dim fractal
        # de cada subimagen
        print ((i*100*1.0)/win5[-1]), "% de datos procesados de 100%"
        # se adiciona la dimensión fracal genreada
        # a la lista ImgFrac    
        ImgText.append(PromH)
    # se transforma la lista ImgFrac en arreglo
    ImgText=np.asarray(ImgText)
    # se redimensiona el arreglo, asemajandolo a la
    #forma M-(w-1),M-(w-1)
    ImgText=np.reshape(ImgText,(win5[-2]-(win5[-3]-1),win5[-2]-(win5[-3]-1)))
    # se guarda la imagen
    gdalnumeric.SaveArray(ImgText,outimg,format="GTiff", prototype=img)
    salida=outimg
    ImgTextSalida=gdal.Open(salida, gdal.GA_Update)
    ImgTextSalida.SetGeoTransform([NewOriginX,PixelWidth,0,NewOriginY,0,PixelHeight])
    # se obtiene el promedio de valores de los pixeles
    print "Promedio:",np.mean(ImgText)
    print "Valor maxmo", np.max(ImgText)
    print "Valor minimo", np.min(ImgText)
    end=time.time()
    print "Total tiempo de procesamiento>>>",(((end-start)*1.0)/60)," Minutos"
    
    return
