# -*- coding: utf-8 -*-
"""
subImages.py
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Created on Mon Jul 06 19:54:47 2015

@author: DAVID
"""
import numpy as np

def subImages(img2array,M,tam):
    """Función que realiza la división de la imágen en subimagenes de tmaño LxL, a las cuales se le realizara el 
    conteo de cajas"""
    # se crea una lista en donde se almacenaran las ventanas generadas en forma de
    # arreglos, en otras palabras es una lista de arreglos
    lista1=list()
    # bucle generado para ir por cada fila tomando cada indice
    for i in range(M)[::tam]:
        # bucle generado para ir por cada columna tomando cada indice
        for j in range(M)[::tam]:
            # se crean las siguientes variables con valor igual a indice i,j
            # fila, columna
            fi=i
            fj=j
            # se crea la subventana (sub arreglo), con los valores inicializados,
            # adicionandole a cada indice el salto siguiente al tamaño de ventana
            # para que vaya generando ventana a ventana
            sub=img2array[fi:fi+tam,fj:fj+tam]
            # se halla el tamaño de la subventana generada
            Nsub,Msub=sub.shape
            # se tiene el condicional que si la ventana generada no es cuadrada
            # no se tendra en cuenta, a demas que debe ser igual al tamaño asignado
            # al principio de la funcion
            if Nsub==tam==Msub:# en la original utiliza todo
                # si cumple con la condicion se adiciona la subimagen a la lista
                lista1.append(sub)
    # se crea un array en donde iran almacenadas todas las ventans generadas para
    # el tamaño dado
    windows=np.asarray(lista1) 
    # se halla la cantidad de ventanas que cumplen con las necesidades y se almacena
    # el valor de la cantidad de ventanas en una variable
    num_ventanas=len(windows)
    # por ultimo la funcion retornara un arreglo con las subventanas (subarreglos)
    # generadas, y el valor de la cantidad de ventanas.
    return windows, num_ventanas