# -*- coding: utf-8 -*-
"""
DBC.py Script that calculate a fractal image with method DBC of Chaudhuri et al. improved by Liu et al. of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Created on Mon Jul 06 19:54:47 2015
Modulo para realizar el calculo de conteo de cajas. Necesario para el modulo ImgFrac.
@author: DAVID
"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as spstats
from subImages import subImages

def Pts(PtsXcaj,u,Lp,nL,ND):
    """Función que calcula la desidad de puntos dentro de cada caja, para obtener la dimensión multifractal"""
    Cmax=1.0*u/Lp
    Cmin=Cmax-np.int(1.0/Lp)
    for caj in range(np.int(nL)):
        A=list()
        for I in ND:
            for J in I:
                if J<=Cmax and J>Cmin:
                    g=J
                    dif=g-Cmin
                    A.append(dif)
                elif Cmin==0:
                        dif=1
                        A.append(dif)
        Ptos_Caj=np.sum(np.asarray(A))
        Cmax=Cmax-(1/Lp)
        Cmin=Cmin-(1/Lp)
        PtsXcaj.append(Ptos_Caj)    
    return PtsXcaj

def DBCxSizWin(num_ventanas,arr_ven,Lp,q):
    """Función que realiza el conteo de cajas de la imagen a un tamaño de subventana dado.\n
    """
    # se crea una lista para almacenar la cantidad de cajas total para
    # cubrir la imagen
    Nl=list()
    PtsXcaj=list()
    uB=list()
    # bucle generado para ir ventana a ventana haciendo el proceso dado
    for i in range(num_ventanas):
        # se crean variables las cuales devolveran el maximo y minimo valor
        # ( en este caso Nivel digital) del sub arreglo o subventana dada
        # se tiene en cuenta que se debe exceptuar el valor cero.
        #print "esta es la ventana ",i+1, "de ",num_ventanas
        r,c=arr_ven[i].shape
        ND=arr_ven[i]
        min_ND=np.min(ND)
        max_ND=np.max(ND)
        # se determina el numero de la caja superior la cual encapsula el
        # maximo nivel digital en la ventana
        u=np.ceil(max_ND*1.0*Lp)#caja superior
        # se determina el numero de la caja inferior la cual encapsula el
        # minimo nivel digital en la ventana       
        #se determina la cantidad de cajas necesarias para cubrir la ventana i
##############################################################################
        #Bloque conteo de cajas original DBC
#        if min_ND==0:
#            b=1  #IMPROVED DS
#            if max_ND==0:
#                u=1
#        else:
#            b=np.ceil(min_ND*1.0*Lp)#caja inferior
#        #nL=(u-b)+1   
##############################################################################
        #Bloque correspondiente al mejoramiento hecho por Liu et al.                                                                           
        if max_ND==min_ND:                            #IMPROVED
            nL=1                                      #IMPROVED
        else:                                         #IMPROVED
            nL=np.ceil((max_ND-min_ND+1)*1.0*Lp)      #IMPROVED  
##############################################################################
        if q!=0:
            PtsXcaj=Pts(PtsXcaj,u,Lp,nL,ND)
        # se adiciona dicha cantidad a la lista Nl, y asi para las demas ventanas
        Nl.append(nL)
    uB.append(np.asarray(PtsXcaj))
    uB=np.asarray(uB)
    uA=np.sum(uB)
    # por ultimo ya teniendo todas las contribuciones dentro de la lista Nl, se 
    # se procede a sumar los valores dentro de dicha lista para obtener el numeor 
    # de cajas total en la imagen
    Nll=np.sum(Nl)
    # la función retornara la cantidad de cajas para recubrir toda la imagen
    # uno de los parametros necesarios para encontrar la DF.
    return  Nll, Nl, uB, uA

def EstDimFrac(img,q):
    """Función que realiza el calculo de la dimensión fractal"""
    img2array=img
    N,M= img2array.shape
    G=np.max(img2array)+1
    lista_Nls=list()
    lista_L=list()
    lista_W=list()
    # bucle que va desde 2 hasta la mitad del tamaño de la imagen mas 1
    # determina el tamaño de la subventana o grilla
    for tam in range((M/2)+1)[2::]:
        # se llama a la funcion y se le ingresa los valores especificados 
        # en este caso 
        if M%tam==0:
            subImg=subImages(img2array,M,tam)
            arr_ven=subImg[0]
            num_ventanas=subImg[1]
            L=tam
            ML=(M*1.0/L)
            Lpt=np.ceil(G*1.0/ML)
            Lp=1.0/Lpt
            Nr=DBCxSizWin(num_ventanas,arr_ven,Lp,q)
            lista_Nls.append(Nr[0])
            Pr=(1.0*Nr[2])/(1.0*Nr[3])
            if q==1:
                numerador=np.sum(1.0*np.log(1.0*((Pr*1.0)**Pr)))
            else:
                numerador=np.log(1.0*np.sum((1.0*Pr)**q))
            lista_W.append(numerador)
            lista_L.append(L)
    Nl=np.asarray(lista_Nls)
    Nl_MF=np.asarray(lista_W)
    L=np.asarray(lista_L)
    r_inv=M*1.0/L
    x=np.log(r_inv)
    y=np.log(Nl)
    DF=spstats.linregress(x,y)
    x2=np.log(L)
    y2=(Nl_MF)
    DMF=spstats.linregress(x2,y2)
    DMFF=DMF[0]
    if q!=1:
        DMFF=DMFF/(q-1)
    return DF,x,y,DMFF

def plotLinReg(x,y,DF,outFile="PlotReg.png"):
    """Función que calcula la dimensión fractal global de la imagen y genera el 
    grafico log-log de la regresión lineal """
    slope=DF[0]
    intercept=DF[1]
    tD=np.linspace(np.min(x),np.max(x))
    pH=slope*(tD)+intercept
    ec= "y="+np.str(np.round(slope,2))+"x+"+str(np.round(intercept,2))+"  DFG="+np.str(np.round(slope,2))
    plt.plot(x,y,".")
    plt.plot(tD,pH,'r-')
    plt.grid()
    plt.xlabel("Ln(1/r)")
    plt.ylabel("Ln(Nl)")
    plt.legend(["Datos",ec],loc=4)
    plt.title("Diagrama Log-Log DBC")
    plt.savefig(outFile,dpi=200,format="PNG")
    return
