# -*- coding: utf-8 -*-
"""
ImgFrac.py Script that calculate a fractal image with method DBC of Chaudhuri et al. improved by Liu et al. of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Created on Wed Jul 15 23:07:22 2015

@author: DAVID
"""
import numpy as np
from DBC import EstDimFrac, plotLinReg
from Algs_Text.WIN.Windowing import windowing
import gdalnumeric
import gdal
import time
def ImgFrac(img, w, q=0, outimg="OutImgFrDBC.tif"):
    """Funcion que generará la imagen fractal o multifractal de la imagen ingresada, por medio 
    del metodo DBC mejorado por Liu et al., recibiendo como parametros la imagen, el tamaño de ventana, el momento q y el nombre del archivo de salida \n
    El tamaño de la ventana movible debe ser par.\n
    Para determinar las imagenes multifractales de orden D1, D2, ... se debe especificar el valor de q correspondiente es decir 1,2,...\n
    La imagen fractal será igual a calcular la imagen multifractal del momento q=0"""
    start=time.time()    
    srcImg=gdal.Open(img)
    geoTrans=srcImg.GetGeoTransform()
    OriginX=geoTrans[0]
    OriginY=geoTrans[3]
    PixelWidth=geoTrans[1]
    PixelHeight=geoTrans[5]
    NewOriginX=1.0*((((w*1.0)/2.0)*1.0)*1.0*PixelWidth)+OriginX
    NewOriginY=1.0*((((w*1.0)/2.0)*1.0)*1.0*PixelHeight)+OriginY
    win5=windowing(img,w)
    win5_arrays=win5[1]
    ImgFrac=list()
    for i in range(win5[-1]):
        Df=EstDimFrac(win5_arrays[i],q)
        if q!=0:
            print ((i*100*1.0)/win5[-1]),"% de datos procesados de 100%"
            ImgFrac.append(Df[-1])
        else:
            print ((i*100)/win5[-1]),"% de datos procesados de 100%"            
            ImgFrac.append(Df[0][0])
    ImgFrac=np.asarray(ImgFrac)
    ImgFrac=np.reshape(ImgFrac,(win5[-2]-(win5[-3]-1),(win5[-2]-(win5[-3]-1))))
    gdalnumeric.SaveArray(ImgFrac,outimg,format="GTiff", prototype=img)
    salida=outimg
    ImgFracSalida=gdal.Open(salida, gdal.GA_Update)
    ImgFracSalida.SetGeoTransform([NewOriginX,PixelWidth,0,NewOriginY,0,PixelHeight])
    print "media",np.mean(ImgFrac)
    print "max DF",np.max(ImgFrac)
    print "min DF",np.min(ImgFrac)
    end=time.time()
    print " Tiempo total de procesamiento >>> ",(((end-start)*1.0)/60)," Minutos"
    return
    
def FracGlob(img,q,outFile="PlotRegDBC.png"):
    """Función que calcula la Dimensión Fractal Global de una imagen por medio del metodo DBC mejorado de Liu et al. y genera el grafico log-log 
    de la regresión lineal"""
    start=time.time()
    img2arr=gdalnumeric.LoadFile(img)
    DFG=EstDimFrac(img2arr,q)
    # Grafico Log-Log recta que se ajusta a la distribución de puntos.
    plotLinReg(DFG[1],DFG[2],DFG[0],outFile)
    print "Dimension fractal Global de la imagen es:",DFG[0][0]
    print "Dimension Multi-fractal Global de la imagen es:",DFG[-1]
    end=time.time()
    print " Tiempo total de procesamiento >>> ",(((end-start)*1.0)/60)," Minutos"
    return
