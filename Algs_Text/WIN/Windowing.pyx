# -*- coding: utf-8 -*-
"""
Windowing.pyx Script that calculate moving window of a image of remote sensing of one band
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Wed Sep 09 13:14:01 2015

@author: DAVID
"""

import gdalnumeric
import numpy as np
# se crea la siguiente función que 
# realizara el proceso de ventaneo o kerneling o
# wndowing sobre la imagen, recibe como parametro
# la imagen, y el tamaño de sub imagen deseado
def windowing(img,w_size=3):
    """Esta Funcion que se utiliza en el calculo de 
la imagen fractal con el metodo TPSA,
esta función realiza el deslizamiento de
las subimagenes sobrepuestas sobre la imagen total retorna arreglo de la imagen [0], arreglo de las subventanas[1]"""
    # se convierte la imagen en arreglo
    img_array=gdalnumeric.LoadFile(img)
    # se obtiene las dimensiones de la imagen
    N,M=img_array.shape
    # se crea una matriz con las mismas dimensiones
    # o tamaño de la subventana deseada
    b=np.zeros((w_size,w_size))

    # se crea una lista donde se almacenaran
    # las subimagenes creadas
    img_w=list()
    # se inicializa la variable d que llevara la
    # cuenta de la cantidad de subimagenes
    d=0
    # primer bucle que ira
    #por cada fila
    for n in range(N-(w_size-1)):
        # segundo bucle que ira por cada columna
        for m in range(M-(w_size-1)):
            # tercer bucle que ira por cada columna 
            # de la subimagen
            for i in range(w_size):
                # cuarto bucle que ira por cada
                # fila de la subimagen
                for j in range(w_size):
                        # se asignan los valores 
                        # i y j a f y c respectivamente

                        f=i
                        c=j
                        # se asigna el valor a cada
                        # pixel del arreclo b
                        b[i,j]=img_array[f+n,c+m]

            # se adiciona uno al valor de d
            # que sera el numeor de subimagen
            d=d+1
            # convertir en numpy array la lista b
            c=np.array(b)
            # adicionar c a la lista img_w
            img_w.append(c)
    #convertir en arreglo la lista img_w
    img_w_array=np.asarray(img_w)
    #retornar arreglo de la imagen, arreglo de las
    # subimagenes generadas, tamaño de la subimagen
    #tamaño de la imagen, cantidad de subimagenes
    return img_array,img_w_array,w_size,M,d