# -*- coding: utf-8 -*-
"""
TPSA.py Script that calculate a fractal image with method TPSA of Clarke of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Mon Jul 13 23:09:31 2015
Funcion calcula la dimendión fractal de una
imagen tamaño MxM, por medio del metodo TPS
@author: DAVID
"""
import matplotlib.pyplot as plt
import numpy as np
from CalcArea import AreaPrism
from subImages import subImages
import scipy.stats as spstats
# se define la función del calculo de
# dimensión fractal, recive como parametros
# la imagen, y el numero de pasos
def TPSA(img,step):
    """Esta funcion calcula la dimendión fractal de una
imagen tamaño MxM, por medio del metodo TPSA, recibe como parametros la imagen
como arreglo y el numero de pasos o iteraciones """
    # se crea una lista vacia, en donde se almacenan los pasos
    list_step=list()
    # se crea una lista la cual almacenara el area del prisma para el paso dado
    list_areaXstep=list()
    # bucle que va por cada valor de paso desde 0 hasta el valor step
    for st in range(step):
        # luego se aplica la funcion subImages a la imagen img, para dividir la imagen
        # recibiendo como parametros la imagen, st= el paso y el nuemor de pasos
        # total indicado por el usuario
        im=subImages(img,st,step)
        # se obtienen el valor [1] de la función subimages, el cual 
        # son las ventanas dentro de un arreglo
        win2array=im[1]# ventanas
        # se obtiene el numero de ventanas total generado
        cant_win=im[2]# numero de ventanas
        # se crea una lista donde se almacenara el area de la imagen, para cada 
        # paso
        Area_img=list()
        # bucle generado que ira ventana a ventana
        for i in range(cant_win):
            # se obtienen las dimensiones de la ventana i=1,2...
            r,c=win2array[i].shape
            # se aplica la función AreaPrism que calculara el area del prisma 
            # para la ventana i, recibe como parametro la ventana, y el paso
            prism=AreaPrism(win2array[i],st)
            # se obtiene la posición [1] de la función AreaPrism, que es 
            # la longitud de la base del prisma
            T_base=prism[1]
            # Se obtiene la posición [0] de ña función AreaPrism, que es
            # el area del prisma
            A_prism_up=prism[0]
            # se almacena el area del prisma de la ventana i en la lista de areas de la imagen
            Area_img.append(A_prism_up)
        # se suman todas las areas de todos los prismas dentro del intervalo cant_win
        # y se obtiene el area total de la imagen
        Area_img=np.sum(np.asarray(Area_img))
        # se adiciona la longitud de las bases de los prismas, en la lista list_step
        list_step.append(T_base)
        # se adiciona el area total de la imagen para la longitud base dada
        list_areaXstep.append(Area_img)
    # se realiza el ajuste por minimos cuadrados de las longitudes de las bases al
    # cuadrado contra los areas
    x=np.log(np.asarray(list_step)**2)
    y=np.log(np.asarray(list_areaXstep))
    regLin=spstats.linregress(x,y)
    # se obtiene la pendiente
    m=regLin[0]
    # la dimensión fractal es igual a la dim topologica menos el valor de la pendiente
    DF=2.0-m
    # retorna el valor de la Dimensión fractal de la imagen
    return DF, regLin, x,y
    
def plotLinReg(x,y,DF,outFile="PlotReg.png"):
    """Función que calcula la dimensión fractal global de la imagen y genera el 
    grafico log-log de la regresión lineal """
    slope=DF[0]
    intercept=DF[1]
    tD=np.linspace(np.min(x),np.max(x))
    pH=slope*(tD)+intercept
    ec= "y="+np.str(np.round(slope,2))+"x+"+str(np.round(intercept,2))+"\n  DFG="+np.str(np.round((2-slope),2))
    plt.plot(x,y,".")
    plt.plot(tD,pH,'r-')
    plt.grid()
    plt.xlabel("Ln(d$^2$)")
    plt.ylabel("Ln(Area)")
    plt.legend(["Datos",ec])
    plt.title("Diagrama Log-Log TPSA")
    plt.savefig(outFile,dpi=200,format="PNG")
    return