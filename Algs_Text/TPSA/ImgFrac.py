# -*- coding: utf-8 -*-
"""
ImgFrac.py Script that calculate a fractal image with method TPSA of Clarke of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Wed Jul 15 23:07:22 2015
Función que calcula la imagen 
fractal
@author: DAVID
"""
import numpy as np
from TPSA import TPSA, plotLinReg
from Algs_Text.WIN.Windowing import windowing
import gdalnumeric
import gdal
import time
# se llama la imagen a procesar
def ImgFrac(img,w,it,outimg="OutImgFrTPSA.tif"):
    """Esta función calcula la imagen fractal de una imagen SAR tamaño MxM
    recibe como parametros la imagen img, el tamaño de ventana w y el numero
    de pasos o iteraciones deseadas \n
    Para escoger el nuemro de iteraciones se debe tener en cuenta el
    siguiente ejemplo:\n
    Por ejemplo, si se escoge un tamaño de ventana igual a 9, el numero de 
    iteraciones estara dado por la siguiente relación. \n
    iteración---relación---tamaño de ventana\n
    1.---------2^0+1=1 ------1\n
    2.---------2^1+1=3-------3\n
    3.---------2^2+1=5-------5\n
    4.---------2^3+1=9-------9\n
    
    Entonces el numero maximo de iteraciones maximas para un tamaño de ventana
    igual a 9x9 será 4"""
    
    start=time.time()
    srcImg=gdal.Open(img)
    geoTrans=srcImg.GetGeoTransform()
    OriginX=geoTrans[0]
    OriginY=geoTrans[3]
    PixelWidth=geoTrans[1]
    PixelHeight=geoTrans[5]
    NewOriginX=1.0*(((w-1.0)/2.0)*1.0*PixelWidth)+OriginX
    NewOriginY=1.0*(((w-1.0)/2.0)*1.0*PixelHeight)+OriginY
    # se llama a la función windowing 
    # que generara las subimagenes
    win5=windowing(img,w)
    # se obtiene las subimagenes
    win5_arrays=win5[1]
    # se crea una lista 
    ImgFrac=list()
    # bucle que ira por cada subimagen
    for i in range(win5[-1]):
        # se calcula la DF de cada subimagen,
        # recibe como parametro la subventana
        # y el numero de pasos
        Df=TPSA(win5_arrays[i],it)
        # se imprime el valor de la dim fractal
        # de cada subimagen
        print ((i*100)/win5[-1]),"% de datos procesados de 100%"  
        # se adiciona la dimensión fracal genreada
        # a la lista ImgFrac    
        ImgFrac.append(Df[0])
    # se transforma la lista ImgFrac en arreglo
    ImgFrac=np.asarray(ImgFrac)
    # se redimensiona el arreglo, asemajandolo a la
    #forma M-(w-1),M-(w-1)
    ImgFrac=np.reshape(ImgFrac,(win5[-2]-(win5[-3]-1),win5[-2]-(win5[-3]-1)))
#    print ImgFrac
    # se guarda la imagen
    gdalnumeric.SaveArray(ImgFrac,outimg,format="GTiff", prototype=img)
    salida=outimg
    ImgFracSalida=gdal.Open(salida, gdal.GA_Update)
    ImgFracSalida.SetGeoTransform([NewOriginX,PixelWidth,0,NewOriginY,0,PixelHeight])
    # se obtiene el promedio de valores de los pixeles
    print "Promedio:",np.mean(ImgFrac)
    print "Valor maxmo DF Local", np.max(ImgFrac)
    print "Valor minimo DF Local", np.min(ImgFrac)
    end=time.time()
    print "Total tiempo de procesamiento>>>",(((end-start)*1.0)/60)," Minutos"
    return
def FracGlob(img,it,outFile="PlotRegTPSA.png"):
    """El número maximo de iteraciones debe cumplir la siguiente relación\n
    iteración---relación---tamaño de imagen menor o igual a\n
    1.---------2^0+1=1 ------1\n
    2.---------2^1+1=3-------3\n
    3.---------2^2+1=5-------5\n
    4.---------2^3+1=9-------9\n 
    .\n       
    .\n
    .\n
    8.--------2^7+1=129----129\n
    por ejemplo si el tamaño de la imagen es 128x128 el numero maximo de 
    iteraciones será 8."""
    start=time.time()
    arrImg=gdalnumeric.LoadFile(img)
    DimFracGlo=TPSA(arrImg,it)
    plotLinReg(DimFracGlo[-2],DimFracGlo[-1],DimFracGlo[-3],outFile)
    print "Dimensión fractal Global de la imagen es", DimFracGlo
    end=time.time()
    print " Tiempo total de procesamiento >>> ",(((end-start)*1.0)/60)," Minutos"
    return