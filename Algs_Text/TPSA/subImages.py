# -*- coding: utf-8 -*-
"""
subImages.py 
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
Created on Mon Jul 06 19:54:47 2015
Funcion que realizara la subdivisión de la imagen.
@author: DAVID
"""
import numpy as np

def subImages(img2array,n,step):
    """Función que realiza la division de la imagen en subimagenes de tamaño LxL, recibe como
    parametros el arreglo de la imagen, el nivel de paso n, la cantidad de pasos step"""
    # se define una variable la cual redimensionara la imagen de entrada en un arreglo de la forma 2**step+1 x 2**step+1
    N,M=img2array.shape
    # se define el tamaño de la ventana, debe ser un pixel mayor a la base del prisma
    tam=2**n+1    
    # se crea una lista en donde se almacenaran las ventanas generadas en forma de
    # arreglos, en otras palabras es una lista de arreglos
    lista1=list()
    # bucle generado para ir por cada fila tomando cada indice
    for i in range(M)[::tam-1]:
        # bucle generado para ir por cada columna tomando cada indice
        for j in range(M)[::tam-1]:
            # se crean las siguientes variables con valor igual a indice i,j
            # fila, columna
            fi=i
            fj=j
            # se crea la subventana (sub arreglo), con los valores inicializados,
            # adicionandole a cada indice el salto siguiente al tamaño de ventana
            # para que vaya generando ventana a ventana
            sub=img2array[fi:fi+tam,fj:fj+tam]
            # se halla el tamaño de la subventana generada
            Nsub,Msub=sub.shape
            # se tiene el condicional que si la ventana generada no es cuadrada
            # no se tendra en cuenta, a demas que debe ser igual al tamaño asignado
            # al principio de la funcion
            if Nsub==tam==Msub:
                # si cumple con la condicion se adiciona la subimagen a la lista
                lista1.append(sub)
    # se crea un array en donde iran almacenadas todas las ventans generadas para
    # el tamaño dado
    windows=np.asarray(lista1) 
    # se halla la cantidad de ventanas que cumplen con las necesidades y se almacena
    # el valor de la cantidad de ventanas en una variable
    num_ventanas=len(windows)
    # por ultimo la funcion retornara un arreglo con la imagen como arreglo,
    # las subventanas (subarreglos) generadas dentro de un arreglo
    # llamado windows, el valor de la cantidad de ventanas 
    # y el tamaño de la imagen.
    return img2array,windows, num_ventanas, M

    