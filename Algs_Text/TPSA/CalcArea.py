# -*- coding: utf-8 -*-
"""
CalcArea.py Script that calculate a fractal image with method TPSA of Clarke of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Tue Jul 07 15:51:02 2015
Esta función calculara el area de cada prisma,
de un paso dado
@author: DAVID
"""
import numpy as np
# se crea una función que calculara el area del prisma recibe como parametro
# el arreglo de la ventana
def AreaPrism(img2array,n):
    """Función que calcula el area del prisma el cual recibe como parámetro el arreglo de la ventana
    y el numero de pasos n"""
    #creamos una variable con el valor del arreglo ingresado 
    ventana=img2array
    # se obtienen las dimensiones del arreglo
    N2,M2=ventana.shape
    #se define la longitud del cuadrado base del prisma
    s=2**n
    # se calculan los vertices de la base del prisma
    a=np.float(ventana[0,0])
    b=np.float(ventana[0,-1])
    c=np.float(ventana[-1,-1])
    d=np.float(ventana[-1,0])
    # se calcula el pixel central
    e=(a+b+c+d)/4
    #-----------------------------------------------------
    ############# Se obtiene el valor del lado AD
    dist_ad=np.sqrt((a-d)**2+s**2)
    ############# Se obtiene el valor del lado AB
    dist_ab=np.sqrt((a-b)**2+s**2)
    ############# Se obtiene el valor del lado CB
    dist_cb=np.sqrt((c-b)**2+s**2)
    ############# Se obtiene el valor del lado CD
    dist_cd=np.sqrt((c-d)**2+s**2)
    #############################################
    #-----------------------------------------------------
    base_e=s/np.sqrt(2)
    ############# Se obtiene el valor del lado EA
    dist_ea=np.sqrt((a-e)**2+(base_e)**2)
    ############# Se obtiene el valor del lado EB
    dist_eb=np.sqrt((b-e)**2+(base_e)**2)
    ############# Se obtiene el valor del lado EC
    dist_ec=np.sqrt((c-e)**2+(base_e)**2)
    ############# Se obtiene el valor del lado ED
    dist_ed=np.sqrt((d-e)**2+(base_e)**2)
    # Se almacenan los valores de los lados en arreglos que 
    # represesentan los cuatro triangulos A,B,C,D
    side_T_A=np.array([dist_ab,dist_ea,dist_eb])
    side_T_B=np.array([dist_cb,dist_eb,dist_ec])
    side_T_C=np.array([dist_cd,dist_ec,dist_ed])
    side_T_D=np.array([dist_ad,dist_ed,dist_ea])
    # en otro arreglo se almacenan los cuatro arreglos anteriores
    prism=np.asarray([side_T_A,side_T_B,side_T_C,side_T_D])
    # Se crea una lista en donde se almacenaran las areas de los 4 triangulos
    # de cada prisma
    list_Areas_T=list()
    # el siguiente bucle ira por cada elemento en el arreglo prisma
    for i in range(len(prism)):
        # variable que traera el valor de lado 0 para el triangulo i dentro del
        # arreglo prisma 
        ab=prism[i][0]
        # variable que traera el valor de lado 1 para el triangulo i dentro del
        # arreglo prisma 
        ea=prism[i][1]
        # variable que traera el valor de lado 2 para el triangulo i dentro del
        # arreglo prisma 
        eb=prism[i][2]
        # variable que luego se le aplicara la raiz para encontrar la altura
        # del triangulo, utiliza como variables los lados del triangulo
        zz=ea**2-(((ea**2)-(eb**2)+(ab**2))/(2*ab))**2
        cg=np.sqrt(zz)
        # se halla el area del triangulo dado
        areaT=(ab*cg)/2
        # se adiciona el area de dicho triangulo a la lista de areas de triangulos
        # del prisma dado
        list_Areas_T.append(areaT)
    # se tiene la sumatoria de las areas de los triangulos, cuyo valor total
    # representara el area de la superficie del prisma dado
    Area_prism=np.sum(np.asarray(list_Areas_T))
    # la funcion retornara el Area del prisma y el tamaño de la base del prisma    
    return Area_prism,s
