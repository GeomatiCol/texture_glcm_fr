# -*- coding: utf-8 -*-
"""
EscImg.py Script that scale a image of remote sensing to resolution of 8bit.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Thu Aug 13 17:17:25 2015
libreria para preparar imagen
@author: DAVID
"""
import gdal
import gdalnumeric
from skimage.exposure import rescale_intensity as ri

def Img(img,b,imgOut="Salida.tif"):
    """Función que escaliza la imagen a 256 niveles de gris y la almacena en tipo 8 bits \n
    Recibe como parametros el nombre del archivo de la imagen (img) y la banda deseada (b) y el 
    nombre del archivo de salida (imgOut)"""
    # se registran todos los drivers
    gdal.AllRegister()
    # la siguiente linea convierte la imagen en un arreglo gdalnumeric = numpyarray
    load=gdalnumeric.LoadFile(img)
    # se toma la ultima banda que corresponde a la imagen
    # Radarsat
    clip=load#[b]
    g=ri(clip,out_range=(0,2)).astype(gdalnumeric.numpy.uint8)
    # se almacena la imagen normalizada
    gdalnumeric.SaveArray(g,imgOut,format="GTiff",prototype=img)
    return
