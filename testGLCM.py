# -*- coding: utf-8 -*-
"""
testGLCM.py Script that calculate a GLCM image of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Thu Aug 13 17:17:25 2015
Archivo para realizar el procedimiento de obtención de imagen de textura metodo GLCM
@author: DAVID
"""
# se importa la libreria GLCM que creara la imagen de textura
from Algs_Text.GLCM.GLCM import GLCM
# se creara una variable donde almacenara el nombre del archivo de la imagen ya escalada
# generada en el archivo test.img
img="Clip.tif"
# se llama la función y se le ingresan los parametros solicitados
TextGLCM=GLCM(img,15,est='correlation',outimg="B1GLCMText_correl_15.tif")
# la imagen de textura se almacenara en la misma dirección del presente archivo

