# -*- coding: utf-8 -*-
"""
testDF_TPSA.py Script that calculate a fractal image with method TPSA of Clarke of a image of remote sensing of one band.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Thu Aug 13 17:17:25 2015
Archivo para realizar el procedimiento de obtención de imagen fractal (de textura) metodo DBC.
@author: DAVID
Created on Thu Aug 13 17:17:25 2015
Archivo para realizar el procedimiento de obtención de imagen fractal (de textura) metodo TPSA
@author: DAVID
"""
# se importa la libreria TPSA que creara la imagen fractal
from Algs_Text.TPSA.ImgFrac import ImgFrac,FracGlob
# se creara una variable donde almacenara el nombre del archivo de la imagen ya escalada
# generada en el archivo test.img
img="Clip.tif"
# se llama la función ImgFrac que es la que generara la imagen
# fractal y se le ingresan los parametros solicitados
TextFrac=ImgFrac(img,7,3, "TPSA_textFr_7_3.tif")
# si se desea se llama la función FracGlob la cual calculara la Dimensión fractal global
#DFG=FracGlob(img,15)
# la función ImgFrac genera la imagen de textura con los parametros especificados
# y la almacena en la misma dirección de este archivo
# a su vez la fucnión FracGlob generara el valor de la dimensión fractal global de la imagen
# el cual lo mostrara en el interprete de python y generara la grafica log-log de la regresión
# el cual almacenara en el mismo directorio de este archivo.