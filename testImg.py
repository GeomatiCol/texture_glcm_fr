# -*- coding: utf-8 -*-
"""
testImg.py Script that scale a image of remote sensing of one band to resolution 8 bits.
Copyright (C) 2016  
David Antonio Suárez Arévalo <dasuareza@correo.udistrital.edu.co> <dasuarez85@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created on Thu Aug 13 17:17:25 2015
Archivo para realizar el procedimiento de escalización de la imagen a 256 niveles de gris.
Si la imagen original ya esta en este rango y con este tipo de dato no será necesario 
realizar este paso.
@author: DAVID
"""
# se importa las libreria Img que escalizara la imagen y almacenara en tipo 8 bits,
# para ser procesada por los algoritmos de textura
from Prep_Img.EscImg import Img
# se creara una variable donde almacenara el nombre del archivo de la imagen
img="TPSA_9_4_Clus_3.tif"
# si la imagen tiene multiples bandas se escoge a continuacuón la banda deseada
# ya que los algoritmos funciónan con imagenes uni-banda
b=-1
# se llama la función y se le ingresan los parametros solicitados
fun=Img(img,b,imgOut="TPSA_9_4_Clus_3_Scale.tif")
# la imagen escalizada aparecera en la misma dirección del presente archivo.

